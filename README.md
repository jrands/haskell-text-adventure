Theme & Backstory - The game takes place on a train in the 1920's. There is a murder on the train, and the player must gather clues and find the culprit before it's too late.

World & Locations - The player starts in a train car, and has the option to go furthur up or down the train. At some points the player may have to go into a side car or even on top of the train.

Items & Objects - Items that the player can find and pickup could include clues to solving the murder, such as a bloody rag or a letter of some sort. There could also be items such as key or a crowbar required to progress.

Goals & Obstacles - The ultimate goal of the game is to solve and apprehend the murderer. Obstacles impeding progress can include locked doors and other passengers.

Essential Types & Classes - Two essential types that will be needed are items and locations. The player needs to have an inventory to store items in, and we need to keep track of their current location. There should also be a World type that keeps track of the state of the game world (i.e. what items were taken and what areas are accessible).

Function Design - The general gameLoop function will most likely take in a world function and return an updated version of the world state. Functions may include things like take, move, and use to allow the user to interact with the game world. 
