import System.IO


data Location = Location { getLocName :: String,
                           getLocDesc :: String,
                           getLocContents :: Maybe Item}

data Player = Player {getPlayerName :: String,
                      getCurrentLoc :: Int,
                      getInventory :: Maybe Item}

data Item = Item { getItemName :: String,
                   getItemDesc :: String} deriving (Eq)

data GameState = GameState { getPlayer :: Player,
                             getLocation :: Location}


data World = World { locList :: [Location]}

item1 = Item "crowbar" "a rusty crowbar"

loc1 = Location "boxcar" "an empty boxcar" (Just item1)

--shows the intro message
displayIntro :: IO()
displayIntro = putStrLn $ "\n~~~~~~~~~~~~~~~~~~~~~~~Welcome to my game!~~~~~~~~~~~~~~~~~~~~~~\n"
                       ++ "\n                                                                \n"
                       ++ "\n----------------------------------------------------------------\n"
                       ++ "\n---------------------------STORY:-------------------------------\n"
                       ++ "\n----------------------------------------------------------------\n"
                       ++ "\nThe year is 1920, and you're on a stream train from New York to \n" 
                       ++ "\nChicago. However, your leisurely trip is interrupted when the   \n"
                       ++ "\nmost dramatic of events occurs: there's been a murder. With only\n"
                       ++ "\na a handful of passengers, tensions are high and no one is safe \n"
                       ++ "\nfrom accusation. It's up to you to explore the train, gather    \n"
                       ++ "\nclues and pinpoint the murderer, before he strikes again...     \n"
                       ++ "\n~You can press h at any time for help and q to quit. Good luck!~\n"

--shows the exit message
displayOutro :: IO()
displayOutro = putStrLn $ "\n------------------------n"
                       ++ "\nThank you for playing!!\n"
                       ++ "\n---©2016 John Randis---\n"
                       ++ "\n-----------------------\n"

--displays to the player every loop with their current location and number of moves
displayState :: GameState -> IO()
displayState g@(GameState p l) = putStrLn ("\nYour current location is " ++ (getLocName (getLocation g)) ++ "\n")

showHelp :: GameState -> IO (GameState)
showHelp g = do 
  putStrLn ("\nPress t to take\nPress d to drop\nPress l to look around your surroundings") 
  return g

--loops through the game
gameLoop :: GameState -> IO()
gameLoop st = do
  cmd <- readInput
  if cmd == 'q' then return ()
  else do
  result <- return st
  (preformAction st) cmd
  displayState st
  gameLoop result

--allows the player to name their character
createChar :: IO(Player)
createChar = do
  hPutStr stderr "\nWhat is your name?\n"
  name <- fmap read getLine
  return $ Player name 0 Nothing

--reads and parses each command
readInput :: IO (Char)
readInput = do
  hPutStr stderr "\nPlease enter a command: "
  cmd <- fmap head getLine
  return cmd

--evalutes what command was entered and preforms that operation
preformAction :: GameState -> Char -> IO GameState
preformAction st cmd 
  | cmd == 't'  = takeItem st
  | cmd == 'd'  = dropItem st
  | cmd == 'h'  = showHelp st
  | cmd == 'l'  = look st
--  | otherwise   = 

--allows the player to add an item from their current location to their inventory
takeItem :: GameState -> IO (GameState)
takeItem g@(GameState player loc) =
  if getLocContents(getLocation g) == Just item1 
  then do 
    putStrLn("You picked up the crowbar")
    return $ g { getPlayer = player{getInventory=Just item1} }
  else do
    hPutStr stderr("Can't pick up item now\n")
    return $ g { getPlayer = player{getInventory=Nothing} }

--removes item from player's inventory    
dropItem :: GameState -> IO (GameState)
dropItem g@(GameState player loc) = 
  if getInventory(getPlayer g) == Just item1
  then do
    putStrLn("You dropped an item")
    return $ g { getPlayer = player{getInventory=Nothing} }
  else do
    hPutStr stderr("Cannot drop that item now\n")
    return $ g

    
--Displays the description of the current location
look :: GameState -> IO(GameState)
look g =  do
  putStrLn ("" ++ (getLocDesc (getLocation g)) ++ "\n")
  return g

--main function
main = do
  displayIntro
  initPlayer <- createChar
  gameLoop (GameState initPlayer loc1)
  displayOutro
  